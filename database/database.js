let mangoose = require('mongoose')
const config = require('../config/database.config')
const { server, database } = config;

class Database {
	constructor(){
		this._connect()
	}

	_connect(){
		mangoose.connect(`mongodb://${server}/${database}`)
		.then(() => {
			console.log('Database connection successful')
		})
		.catch(() => {
			console.error('Database connection error')
		})
	}
}

module.exports = new Database();