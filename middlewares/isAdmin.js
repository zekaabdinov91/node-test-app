const User = require('../models/user.js')
module.exports = function(req, res, next){
	const err = new Error('Forbidden')
	err.status = 403
	User.findOne({ 'token.access_token': req.headers['authorization'] })
	.then(user => {
		if(user.role.indexOf('Admin') !== -1)
			return next()
		else
			return next(err)
	})
	.catch(err => {
		next(err)
	})
}