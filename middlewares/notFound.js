module.exports = (req, res, next) => {
	const err = new Error('Not found');
	err.status = 404
	if (!req.route)
        return next (err);  
    next();
}