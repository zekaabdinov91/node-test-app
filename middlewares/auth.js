const User = require('../models/user.js')
module.exports = (req, res, next) => {
	const err = new Error('Unauthorized')
	err.status = 401
	if(!req.headers['authorization'])
		return next(err)
	User.find({ 'token.access_token': req.headers['authorization'] })
	.then(user => {
		if(!user.length){
			return next(err)
		}
		return next()
	})
	.catch(err => {
		return next(err)
	})

}