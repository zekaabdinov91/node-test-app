const express = require('express')
const app = express()
const db = require('./database/database')
const expressValidator = require('express-validator')
const port = 3000
const catalogRouter = require('./routes/catalog.js')
const userRouter = require('./routes/user.js')
const bodyParser = require('body-parser')
const auth = require('./middlewares/auth.js')
const errorHandler = require('./middlewares/errorHandler.js')
const notFound = require('./middlewares/notFound.js')

app.use(bodyParser.json())
app.use(expressValidator())
app.use('/api/catalog', auth, catalogRouter)
app.use('/api/user', userRouter)
app.use(notFound);
app.use(errorHandler)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

