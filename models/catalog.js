const mongoose = require('mongoose')
const Schema = mongoose.Schema

const localeSchema = new Schema({
	lang : {type: String,required: true},
	title : {type: String, required: true},
	description : {type:String, required:false}
})

const childCatalogSchema = new Schema({
	locale:[localeSchema],
	icon:{type:String, required:false, default:null},
	created_at:{type:Date, default:Date.now()},
	updated_at:{type:Date, default:Date.now()}
})


const parentCatalogSchema = new Schema({
	locale:[localeSchema],
	subCatalogs:[childCatalogSchema],
	icon:{type:String, required:false, default:null},
	created_at:{type:Date, default:Date.now()},
	updated_at:{type:Date, default:Date.now()}
})



module.exports = mongoose.model('Catalog', parentCatalogSchema)