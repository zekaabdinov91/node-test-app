const mongoose = require('mongoose')
const Schema = mongoose.Schema

const tokenSchema = new Schema({
	access_token:{type:String, required: true},
	refresh_token:{type:String, required: true},
	access_token_expire_date:{type:Date, reuqired: true},
	refresh_token_expire_date:{type:Date, reuqired: true}
})
const userSchema = new Schema({
	email:{type: String, required: true},
	password:{type:String, required: true},
	token:[tokenSchema],
	role:[{name: String,id:Number}]
})

module.exports = mongoose.model('User',userSchema)