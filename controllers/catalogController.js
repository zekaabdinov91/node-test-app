const Catalog = require('../models/catalog.js')
const { body, validationResult } = require('express-validator/check')

exports.index = (req, res, next) => {
	Catalog.find()
	.then(catalog => {
		res.send(catalog)
	})
	.catch(err => {
		return next(err)
	})
}
exports.createParentCatalog = (req, res, next) => {
	const errors = validationResult(req)
	if(!errors.isEmpty())
		return res.status(422).json({errors: errors.array()})
	let catalog = new Catalog({
		locale:req.body.locale,
		subCatalogs:[]
	})
	catalog.save()
	.then((catalog) => {
		res.send(catalog)
	})
	.catch((err) => {
		return next(err)
	})
}
exports.createSubCatalog = (req, res, next) => {
	const errors = validationResult(req)
	if(!errors.isEmpty())
		return res.status(422).json({errors: errors.array()})
	Catalog.findById(req.params.parentId)
	.then(catalog => {
		if(!catalog)
			return res.status(404).send({
				message: "catalog not found with id " + req.params.parentId
			})
		return catalog
	})
	.then(catalog => {
		catalog.subCatalogs.push(req.body)
		catalog.save()
		return catalog
	})
	.then(catalog => {
		res.send({
			messsage: 'Saved successfully'
		})
	})
	.catch(err => {
		return next(err)
	})
}
exports.findOne = (req, res, next) => {
	Catalog.findById(req.params.id)
	.then(catalog => {
		if(!catalog)
			return res.status(404).send({
				message: "Note not found with id " + req.params.id
			})
		res.send(catalog)
	})
	.catch(err => {
		return next(err)
	})
}
exports.deleteParentCatalog = (req, res, err) => {
	Catalog.findByIdAndRemove(req.params.id)
	.then(catalog => {
		if(!catalog)
			return req.status(404).send({
				message: "Note not found with id " + req.params.id
			})
		res.send({message: "Catalog deleted successfully!"})
	})
	.catch(err => {
        return next(err)
	})
}
exports.deleteChildCatalog = (req, res, next) => {
	Catalog.findById(req.params.parentId)
	.then(catalog => {
		if(!catalog)
			return res.status(404).send({
				message: "catalog not found with id " + req.params.parentId
			})
		return catalog
	})
	.then(catalog => {
		catalog.subCatalogs.remove({_id: req.params.childId})
		catalog.save()
	})
	.then(catalog => {
		res.send({
			messsage: 'Deleted successfully'
		})
	})
	.catch(err => {
		return next(err)
	})
}
exports.validate = (method) => {
	switch(method){
		case 'createParentCatalog' : {
			return [
				body('locale').exists().isArray(),
				body('locale.*.lang').exists(),
				body('locale.*.title').exists().isLength({min:2}),
				body('locale.*.description').isLength({max: 250}),
			]
		}
		case 'createSubCatalog' : {
			return [
				body('locale').exists().isArray(),
				body('locale.*.lang').exists(),
				body('locale.*.title').exists().isLength({min:2}),
				body('locale.*.description').isLength({max: 250}),
			]
		}
	}
}
