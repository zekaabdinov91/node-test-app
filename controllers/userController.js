const User = require('../models/user.js')
const {body, validationResult} = require('express-validator/check')
const bcrypt = require('bcrypt')
const userTokenGenerator = require('../helpers/helper.js')

exports.index = (req, res, next) => {
	User.find().select({'_id':1,'email':1, 'role':1})
	.then(users => {
		res.send(users)
	})
	.catch(err => {
		return next(err)
	})
}

exports.register = (req, res, next) => {
	const errors = validationResult(req)
	if(!errors.isEmpty())
		return res.status(422).json({errors: errors.array()})
	const saltRounds = 10
	let hash = bcrypt.hashSync(req.body.password, saltRounds)
	let token = userTokenGenerator.generateUserToken()
	let user = new User({
		email: req.body.email,
		password: hash,
		token: [token],
		role:["Registered User"]
	})
	user.save()
	.then(() => {
		res.send({
			access_token: token.access_token,
			refresh_token: token.refresh_token
		})
	})
	.catch((err) => {
		return next(err)
	})
}	

exports.login = (req, res, next) => {
	let token = userTokenGenerator.generateUserToken()
	const errors = validationResult(req)
	if(!errors.isEmpty())
		return res.status(422).json({errors: errors.array()})
	User.findOne({email: req.body.email})
	.then(user => {
		if(!user)
			return res.status(404).send({
                message: "user not found"
            });
		if(bcrypt.compareSync(req.body.password, user.password)) {
			user.token.push(token)
			return user.save()
		} else {
			return res.status(404).send({
                message: "password is not valid"
            });
		}
	})
	.then(user => {
		res.send({
			access_token: token.access_token,
			refresh_token: token.refresh_token
		})
	})
	.catch(err => {
        return next(err)
	})
}

exports.logout = (req, res, next) => {
	const errors = validationResult(req)
	if(!errors.isEmpty())
		return res.status(422).json({errors: errors.array()})
	User.findOneAndUpdate( { 'token.access_token': req.body.access_token }, { $pull : {
		'token' : {'access_token': req.body.access_token}
	}} )
	.then(user => {
		if(!user)
			return res.status(404).send({
				message: 'User not found'
			})
		res.send({
			message: 'Token delete successfully'
		})
	})
	.catch(err => {
        return next(err)
	})
}

exports.refreshToken = (req, res) => {
	// const errors = validationResult(req)
	// if(!errors.isEmpty())
	// 	return res.status(422).json({errors: errors.array()})
	// let token = userTokenGenerator.generateUserToken()
	// User.findOneAndUpdate( { 'token.access_token': req.body.access_token }, {$push: {}} )
}


exports.validate = (method) => {
	switch(method){
		case 'register' :{
			return [
				body('email').exists().isEmail(),
				body('password').exists().isLength({min:6})
			]
		}
		case 'login' : {
			return [
				body('email').exists().isEmail(),
				body('password').exists()
			]
		}
		case 'logout': {
			return [
				body('access_token').exists().isLength({min:60,max:60})
			]
		}
		case 'refreshToken': {
			return [
				body('refresh_token').exists().isLength({min:60,max:60})
			]
		}
	}
}
