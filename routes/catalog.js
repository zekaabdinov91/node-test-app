const express = require('express');
const router = express.Router();
const catalogController = require('../controllers/catalogController.js')
// const auth = require('../middlewares/auth.js')

router.get('/', catalogController.index);
router.get('/:id', catalogController.findOne);
router.post('/',
	catalogController.validate('createParentCatalog'),
	catalogController.createParentCatalog
);
router.post('/createSubCatalog/:parentCatalogId',
	catalogController.validate('createSubCatalog'),
	catalogController.createSubCatalog
);
router.delete('/parent/:id', catalogController.deleteParentCatalog);
router.delete('/child/:parentId/:childId', catalogController.deleteChildCatalog);


module.exports = router