const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController.js')
const isAdmin = require('../middlewares/isAdmin.js')
const auth = require('../middlewares/auth.js')

router.get('/', auth, isAdmin, userController.index)
router.post('/register', userController.validate('register'), userController.register)
router.post('/login', userController.validate('login'), userController.login)
router.post('/logout', userController.validate('logout'), userController.logout)
router.put('/refreshToken', userController.validate('refreshToken'), userController.refreshToken)

module.exports = router
