const crypto = require('crypto')
const moment = require('moment')
exports.generateUserToken = function(){
	let token = {}
	token.access_token = crypto.randomBytes(60).toString('hex')
	token.refresh_token = crypto.randomBytes(60).toString('hex')
	let now = moment()
	token.access_token_expire_date  = moment(now).add(30, 'minutes')
	token.refresh_token_expire_date = moment(now).add(1, 'month')
	return token
}